import reader.*;

// Copyright (c) 2020, Identix identix@identix.com.br
// 
// Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

// Example for simple inventory using a set time
public class ReadTagsTime {
	public static void main(String[] args) {
		try {
			Reader r = new Reader();

			// Get avaiable ports
			String[] ports = r.getAvaiablePorts();

			// As a good practice we will reset the reader to clean up old
			// configurations
			ReaderSettings settings = new ReaderSettings();
			r.resetSettings();

			if (ports.length > 0) {
				// Configure Port
				settings.Port = ports[0];

				// Configure read mode
				settings.SearchMode = SearchMode.DualTarget;

				// Configure session mode
				settings.Session = 0;

				// Configure Power
				settings.TxPower = 10;

				// Apply settings
				r.applySettings(settings);

				// Register the callbacks
				r.setOnCompleteListener(new OnTagReport() {
					@Override
					public void onReport(String data) {
						System.out.println(data);
					}
				});

				// Start inventory
				r.start(1000);				
			} else {
				System.out.println("No ports found!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
