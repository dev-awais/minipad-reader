// Copyright (c) 2020, Identix identix@identix.com.br
// 
// Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
// 
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import java.util.ArrayList;

import reader.*;

// Example on how to write multiple EPC
public class WriteMultipleEpc {
	public static void main(String[] args) {
		try {
			Reader r = new Reader();

			// Get avaiable ports
			String[] ports = r.getAvaiablePorts();

			// As a good practice we will reset the reader to clean up old
			// configurations
			ReaderSettings settings = new ReaderSettings();
			r.resetSettings();

			if (ports.length > 0) {
				// Configure Port
				settings.Port = ports[0];

				// Configure read mode
				settings.SearchMode = SearchMode.DualTarget;

				// Configure session mode
				settings.Session = 0;

				// Configure Power
				settings.TxPower = 23;			

				// Apply settings
				r.applySettings(settings);

				// Register the callbacks
				r.setOnCompleteListener(n8ew OnTagReport() {
					@Override
					public void onReport(String data) {
						System.out.println(data);
					}
				});

				ArrayList<EpcWrite> listWrite = new ArrayList<EpcWrite>();																										
				
			    listWrite.add(new EpcWrite("3035E1967C06F54000000152", "000000000000000000000001"));
				listWrite.add(new EpcWrite("3035E1967C06F280000001E4", "000000000000000000000002"));							
				listWrite.add(new EpcWrite("3035E1967C0802400000009E", "000000000000000000000003"));
				listWrite.add(new EpcWrite("3035E1967C080280000000EC", "000000000000000000000004"));
				listWrite.add(new EpcWrite("3035E1967C070C8000000072", "000000000000000000000005"));												
								
//				listWrite.add(new EpcWrite("000000000000000000000001", "3035E1967C06F54000000152"));
//				listWrite.add(new EpcWrite("000000000000000000000002", "3035E1967C06F280000001E4"));
//				listWrite.add(new EpcWrite("000000000000000000000003", "3035E1967C0802400000009E"));
//				listWrite.add(new EpcWrite("000000000000000000000004", "3035E1967C080280000000EC"));
//				listWrite.add(new EpcWrite("000000000000000000000005", "3035E1967C070C8000000072"));								
				
				r.writeEpc(listWrite);

				for (EpcWrite epcWrite : listWrite) {
					System.out.println(epcWrite.CurrentEpc + " => " + epcWrite.NewEpc + " : "
							+ (epcWrite.IsWrite ? "written" : "unwritten"));
				}
			} else {
				System.out.println("No ports found!");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}