import okhttp3.*;
import reader.*;
import java.util.*;
import java.io.File;  // Import the File class
import java.io.IOException;  // Import the IOException class to handle errors
import java.io.FileWriter;

public class TestMain {
   OkHttpClient client = new OkHttpClient();
   // code request code here
   String doGetRequest(String url) throws IOException {
     Request request = new Request.Builder()
         .url(url)
         .build();

     Response response = client.newCall(request).execute();
     return response.body().string();
   }

   // post request code here

   public static final MediaType JSON
   = MediaType.parse("application/json; charset=utf-8");

   // test data
   String bowlingJson(String player1, String player2) {
          return "{'codes': " + player1 + "}";
       }

   String doPostRequest(String requestUrl, String json) throws IOException {
     HttpUrl.Builder urlBuilder = HttpUrl.parse(requestUrl).newBuilder();
     urlBuilder.addQueryParameter("v", "1.0");
     urlBuilder.addQueryParameter("codes", "vogella");
     String url = urlBuilder.build().toString();

       // RequestBody body = RequestBody.create(JSON, json);
         Request request = new Request.Builder()
             .url(url)
             .build();
         Response response = client.newCall(request).execute();
         return response.body().string();
   }

   public static void main(String[] args) throws IOException {

     // // issue the Get request
     // TestMain example = new TestMain();
     // // issue the post request
     //
     // String json = example.bowlingJson("Jesse", "Jake");
     // String postResponse = example.doPostRequest("http://localhost:3000/addDataToDB", json);
     // System.out.println(postResponse);
   }
 }
