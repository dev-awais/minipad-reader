

# Compile and run on the terminal

1: Open terminal at project folder.
2: Example to compile:  javac -cp jssc-2.8.0.jar:IdentixSerialSDK.jar ReadTags.java
3: Example to run:  java -cp jssc-2.8.0.jar:IdentixSerialSDK.jar:. ReadTags


# Compile and Run with dependencies
1: Open terminal at project folder.
2: To compile code: javac -cp jssc-2.8.0.jar:IdentixSerialSDK.jar:okhttp-3.10.0.jar:okio-1.14.0.jar ReadTags.java
3: To Run code: java -cp jssc-2.8.0.jar:IdentixSerialSDK.jar:okhttp-3.10.0.jar:okio-1.14.0.jar ReadTags.java 


# Reference 
https://idntx.zendesk.com/hc/en-us/articles/360044660314-miniPad-rPad-SDK-Java-for-Linux

