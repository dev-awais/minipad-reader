import okhttp3.*;
import reader.*;
import java.util.*;
import java.io.File;  // Import the File class
import java.io.IOException;  // Import the IOException class to handle errors
import java.io.FileWriter;
//For APIs
// Copyright (c) 2020, Identix identix@identix.com.br
//
// Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

// Example for simple inventory with a read timeout
public class ReadTags {
	private static FileWriter file;
    private static Set<String> dataArray = new HashSet<String>();
		OkHttpClient client = new OkHttpClient();


		// post request code here

		public static final MediaType JSON
		= MediaType.parse("application/json; charset=utf-8");

		String doGetRequest(String url) throws IOException {
      Request request = new Request.Builder()
          .url(url)
          .build();

      Response response = client.newCall(request).execute();
      return response.body().string();
    }

		String doPostRequest(String requestUrl, Set<String> data ) throws IOException {
			String joined = String.join(",", data);
			HttpUrl.Builder urlBuilder = HttpUrl.parse(requestUrl).newBuilder();
			urlBuilder.addQueryParameter("codes", joined);
			String url = urlBuilder.build().toString();
			// Map<String, Set<String>> dictionary = new HashMap<String, Set<String>>();
			// dictionary.put("codes", data);

			// System.out.println(dictionary);

			System.out.println(url);
			// RequestBody body = RequestBody.create(JSON, joined);
					Request request = new Request.Builder()
							.url(url)
							.build();
					Response response = client.newCall(request).execute();
					return response.body().string();
		}

	public static void main(String[] args) {
		dataArray.add("0000000000551");
		// dataArray.add("0000000000552");
		// dataArray.add("0000000000553");
		// dataArray.add("0000000000554");
		// dataArray.add("0000000000555");
		// dataArray.add("0000000000556");
		// uploadDataToServer(dataArray);
		try {
			Reader r = new Reader();
			// Get avaiable ports
			String[] ports = r.getAvaiablePorts();

			// As a good practice we will reset the reader to clean up old
			// configurations
			ReaderSettings settings = new ReaderSettings();
			r.resetSettings();

			if (ports.length > 0) {
				// Configure Port
				settings.Port = ports[0];

				// Configure read mode
				settings.SearchMode = SearchMode.AutoSeach;

				// Configure session mode
				settings.Session = 0;

				// Configure Power
				settings.TxPower = 23;

				// settings.BeeperVolume=0;

				// Apply settings
				r.applySettings(settings);

				// Register the callbacks
				r.setOnCompleteListener(new OnTagReport() {
					@Override
					public void onReport(String data){
						if (dataArray.size() >= 1) {
							// System.out.println("Array size exeeded");
							// createFile(dataArray);
								uploadDataToServer(dataArray);
						}
						int scannedCode;
						try {
							scannedCode = Integer.parseInt(data);
						  if (dataArray.contains(data)) {
							// System.out.println("Already scanned");
						   } else {
							System.out.println(dataArray);
							System.out.println("Scanned: " + data);
							dataArray.add(data);
						   }
						} catch (NumberFormatException e) {
							System.out.println("Scanner logs: " + data);
						}
					}
				});
				int i = 1;
				while (true) {
					sendApplicationStatus();
					System.out.println("INVENTORY " + i);
					// Start inventory
					r.start();
					Thread.sleep(5000);
					// Stop inventory
					r.stop();
					i++;
				}

			} else {
				System.out.println("No ports found!");
				sendDeviceStatus();
				sendApplicationStatus();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			sendDeviceStatus();
		}
	}

	static void createFile(Set<String> data) {
		try {
      		File myObj = new File("codes.txt");
      		if (myObj.createNewFile()) {
     				System.out.println("File created: " + myObj.getName());
					 saveDataToFile(data);
  			} else {
    				 System.out.println("File already exists.");
					 saveDataToFile(data);
    			}
  			 } catch (IOException e) {
   					System.out.println("An error occurred.");
   					e.printStackTrace();
   			}
	}

	static void saveDataToFile(Set<String> data) {
		data.stream().forEach((d) -> {
			try {
				file = new FileWriter("codes.txt", true);
				file.write("\n"+d+"\n");
      			System.out.println("Successfully wrote to the file.");
				file.close();
				dataArray.clear();
    		} catch (IOException e) {
      			System.out.println("An error occurred.");
      			e.printStackTrace();
    		}
		});
	}

	static void uploadDataToServer(Set<String> data) {
		System.out.println("Checking Server connection...");
		// issue the Get request
		ReadTags example = new ReadTags();
		try {
				String postResponse = example.doPostRequest("http://localhost:3000/addDataToDB", data);
				System.out.println(postResponse);
				if (postResponse.contains("success")) {
					dataArray.clear();
				}
			} catch (Exception e) {
				System.out.println("Something bad happend in posting request");
			}
		}

		static void sendDeviceStatus() {
			ReadTags example = new ReadTags();
			try {
				String getRequest = example.doGetRequest("http://localhost:3000/deviceConnectionStatus");
				System.out.println(getRequest);
			} catch (Exception e) {
				System.out.println("Something went wrong in sending device status.");
			}
		}

		static void sendApplicationStatus() {
			ReadTags example = new ReadTags();
			try {
				String getRequest = example.doGetRequest("http://localhost:3000/appStatus");
			} catch (Exception e) {

			}
		}
}
